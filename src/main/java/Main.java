import formation.ws.HelloWorldImpl;

import javax.xml.ws.Endpoint;

class Main {
    public static void main(String[] args) {
        System.out.println("Hello world Dany "+System.getProperty("java.version")+" / " + getVersion());
        Endpoint.publish(
                "http://localhost:8080/helloworldImp",
                new HelloWorldImpl());
    }

    private static int getVersion() {
        String version = System.getProperty("java.version");
        if(version.startsWith("1.")) {
            version = version.substring(2, 3);
        } else {
            int dot = version.indexOf(".");
            if(dot != -1) { version = version.substring(0, dot); }
        } return Integer.parseInt(version);
    }
}