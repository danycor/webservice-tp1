package formation.client;

import formation.IHelloWorld;
import formation.ws.HelloWorldImplService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class HelloWorldClient {
    public static void main(String[] args) throws MalformedURLException {
        HelloWorldImplService service;
        service = new HelloWorldImplService();
        System.out.println(service.getHelloWorldImplPort().getHelloWorldAsString("test dany"));
        clientQname();
    }
    public static void clientQname() throws MalformedURLException {
        URL url = new URL("http://localhost:8080/helloworldImp?wsdl") ;
        QName qName = new QName("http://ws.formation/", "HelloWorldImplService");
        Service ser = Service.create(url, qName);
        IHelloWorld port = ser.getPort(IHelloWorld.class);
        System.out.println(port.getHelloWorldAsString("Test via qname"));
    }

}
