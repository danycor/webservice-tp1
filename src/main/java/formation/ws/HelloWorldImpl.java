package formation.ws;

import formation.IHelloWorld;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(endpointInterface = "formation.IHelloWorld")
public class HelloWorldImpl implements IHelloWorld {
    @WebMethod()
    public String getHelloWorldAsString(String name) {
        return "Rcev name : "+name;
    }
}
